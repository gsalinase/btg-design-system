import Vue from 'vue'
import App from './App.vue'
import vueCustomElement from 'vue-custom-element'

Vue.config.productionTip = false
global.jQuery = global.$ = require('jquery');

Vue.use(vueCustomElement)


// Register all components as Webcomponents
var requireComponent = require.context('../src/components/', true, /.vue$/)
requireComponent.keys().forEach(function (fileName) {
	var baseComponentConfig = requireComponent(fileName)
	baseComponentConfig = baseComponentConfig.default || baseComponentConfig
	if (baseComponentConfig.name) {
		Vue.customElement(baseComponentConfig.name, baseComponentConfig)
	}
})



new Vue({
  render: h => h(App)
}).$mount('#app')
