module.exports = {
  productionSourceMap: false,
  transpileDependencies: ['vuejs-datepicker'],
  baseUrl: '',
	chainWebpack: config => {
    if(config.plugins.has('extract-css')) {
      const extractCSSPlugin = config.plugin('extract-css')
      extractCSSPlugin && extractCSSPlugin.tap(() => [{
        filename: '[name].css',
        chunkFilename: '[name].css'
      }])
    }
  },
  configureWebpack: config => {
    config.externals = {
      '$': 'jquery',
      'moment': 'moment'
    }
    // config.output = {
    //   filename: '[name].js',
    //   chunkFilename: '[name].js'
    // }
  },
  configureWebpack: {
    output: {
      filename: '[name].js',
      chunkFilename: '[name].js'
    }
  }

}